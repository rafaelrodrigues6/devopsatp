require_relative '../app/calculator.rb'

RSpec.describe Calculator do
  describe 'Sum values' do
    it 'must return correct value' do
      expect(Calculator.sum(1, 2)).to eq(3)
      expect(Calculator.sum(10, -2)).to eq(8)
    end
  end

  describe 'Multiply values' do
    it 'must return the correct value' do
      expect(Calculator.multiply(2,2)).to eq(4)
      expect(Calculator.multiply(3,-3)).to eq(-9)
    end
  end

  describe 'Calculate the average' do
    it 'must return the correct value' do
      expect(Calculator.average([1, 2, 3])).to eq(2)
    end
  end
end
