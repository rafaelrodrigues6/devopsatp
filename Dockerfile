FROM ruby:2.6

RUN mkdir /myapp

WORKDIR /myapp
COPY . /myapp

RUN gem install bundler
RUN bundle install
